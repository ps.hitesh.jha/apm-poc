## Pre-requisites

Install the appropriate Docker application for your operating system.

If you’re running on Linux, install Docker Compose.

``sudo sysctl -w vm.max_map_count=262144``

To persist these changes add vm.max_map_count=262144 in /etc/sysctl.conf file

Start Elastic Stack with Docker Compose
To get a single-node Elasticsearch cluster and Kibana up and running in Docker with security enabled, you can use Docker Compose

## Prepare the environment

Create the following configuration files in a new, empty directory.
the .env file sets environment variables that are used when you run the docker-compose.yml configuration file

## .env
```
# Password for the 'elastic' user (at least 6 characters)
ELASTIC_PASSWORD=elastic

# Password for the 'kibana_system' user (at least 6 characters)
KIBANA_PASSWORD=kibana

# Version of Elastic products
STACK_VERSION=8.1.0

# Set the cluster name
CLUSTER_NAME=docker-cluster

# Set to 'basic' or 'trial' to automatically start the 30-day trial
LICENSE=basic
#LICENSE=trial

# Port to expose Elasticsearch HTTP API to the host
#ES_PORT=9200
ES_PORT=127.0.0.1:9200

# Port to expose Kibana to the host
KIBANA_PORT=5601
#KIBANA_PORT=80

# Increase or decrease based on the available host memory (in bytes)
MEM_LIMIT=1073741824

# Project namespace (defaults to the current folder name if not set)
#COMPOSE_PROJECT_NAME=myproject
```

### docker-compose.yml
This docker-compose.yml file creates a single-node secure Elasticsearch cluster with authentication and network encryption enabled, and a Kibana instance securely connected to it.

```
version: "2.2"

services:
  setup:
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - certs:/usr/share/elasticsearch/config/certs
    user: "0"
    command: >
      bash -c '
        if [ x${ELASTIC_PASSWORD} == x ]; then
          echo "Set the ELASTIC_PASSWORD environment variable in the .env file";
          exit 1;
        elif [ x${KIBANA_PASSWORD} == x ]; then
          echo "Set the KIBANA_PASSWORD environment variable in the .env file";
          exit 1;
        fi;
        if [ ! -f certs/ca.zip ]; then
          echo "Creating CA";
          bin/elasticsearch-certutil ca --silent --pem -out config/certs/ca.zip;
          unzip config/certs/ca.zip -d config/certs;
        fi;
        if [ ! -f certs/certs.zip ]; then
          echo "Creating certs";
          echo -ne \
          "instances:\n"\
          "  - name: es01\n"\
          "    dns:\n"\
          "      - es01\n"\
          "      - localhost\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          > config/certs/instances.yml;
          bin/elasticsearch-certutil cert --silent --pem -out config/certs/certs.zip --in config/certs/instances.yml --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key;
          unzip config/certs/certs.zip -d config/certs;
        fi;
        echo "Setting file permissions"
        chown -R root:root config/certs;
        find . -type d -exec chmod 750 \{\} \;;
        find . -type f -exec chmod 640 \{\} \;;
        echo "Waiting for Elasticsearch availability";
        until curl -s --cacert config/certs/ca/ca.crt https://es01:9200 | grep -q "missing authentication credentials"; do sleep 30; done;
        echo "Setting kibana_system password";
        until curl -s -X POST --cacert config/certs/ca/ca.crt -u elastic:${ELASTIC_PASSWORD} -H "Content-Type: application/json" https://es01:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 10; done;
        echo "All done!";
      '
    healthcheck:
      test: ["CMD-SHELL", "[ -f config/certs/es01/es01.crt ]"]
      interval: 1s
      timeout: 5s
      retries: 120

  es01:
    depends_on:
      setup:
        condition: service_healthy
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - certs:/usr/share/elasticsearch/config/certs
      - esdata01:/usr/share/elasticsearch/data
    ports:
      - ${ES_PORT}:9200
    environment:
      - node.name=es01
      - cluster.name=${CLUSTER_NAME}
      - cluster.initial_master_nodes=es01
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es01/es01.key
      - xpack.security.http.ssl.certificate=certs/es01/es01.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.http.ssl.verification_mode=certificate
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es01/es01.key
      - xpack.security.transport.ssl.certificate=certs/es01/es01.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=${LICENSE}
      - xpack.security.authc.api_key.enabled=true

    mem_limit: ${MEM_LIMIT}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert config/certs/ca/ca.crt https://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120


  kibana:
    depends_on:
      es01:
        condition: service_healthy
    image: docker.elastic.co/kibana/kibana:${STACK_VERSION}
    volumes:
      - certs:/usr/share/kibana/config/certs
      - kibanadata:/usr/share/kibana/data
    ports:
      - ${KIBANA_PORT}:5601
    environment:
      - SERVERNAME=kibana
      - ELASTICSEARCH_HOSTS=https://es01:9200
      - ELASTICSEARCH_USERNAME=kibana_system
      - ELASTICSEARCH_PASSWORD=${KIBANA_PASSWORD}
      - ELASTICSEARCH_SSL_CERTIFICATEAUTHORITIES=config/certs/ca/ca.crt
      - xpack.security.enabled=true
      - xpack.encryptedSavedObjects.encryptionKey="dadasddddddddddddddwdffffffffffffffffffffassaaaaaaaaaaaqwqeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
    mem_limit: ${MEM_LIMIT}
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s -I http://localhost:5601 | grep -q 'HTTP/1.1 302 Found'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

volumes:
  certs:
    driver: local
  esdata01:
    driver: local
  kibanadata:
    driver: local
```

## Start your cluster
### 1. Create and start the three-node Elasticsearch cluster and Kibana instance

   `docker-compose up -d`

When the deployment has started, open a browser and navigate to http://localhost:5601 to access Kibana, where you can load sample data and interact with your cluster.

### 2. Check the logs for deployment using

   `docker-compose logs -f`

### 3. Stop and remove the deployment

   `docker-compose down -v`

## Collect Application Performance Monitoring (APM) data and send to the Elastic Stack

### 1. Setup Fleet

Use Fleet in Kibana to get APM data into the Elastic Stack


1. Log in to Kibana and go to Management > Fleet.
2. Click Fleet settings, and in the Fleet Server hosts field, specify the URLs Elastic Agents will use to connect to Fleet Server. For example, http://localhost:8220, where localhost is the host IP where you will install Fleet Server.
3. In the Elasticsearch hosts field, specify the Elasticsearch URLs where Elastic Agents will send data. For example, https://localhost:9200.
4. Save and apply the settings.
5. Click the Agents tab and follow the in-product instructions to add a Fleet server:
6. Navigate to the directory where elastic agent is downloaded during above step and run command to start feet server, a sample is shown below. 
7. sudo ./elastic-agent install  \
   --fleet-server-es=https://localhost:9200 \
   --fleet-server-service-token={token} \
   --fleet-server-policy=fleet-server-policy \
   --fleet-server-es-ca=/path/to/ca.crt

Note: You can get your ca certificates which is generated inside elastic search container in following path 

  ``docker cp elastic-container-id:usr/share/elasticsearch/config/certs /some/path``

Make sure certs have perssion for apm_user using below command:

`chmod -R a+rwX path/to/certs/`

If installation is successful, you’ll see the Fleet Server Elastic Agent on the Agents tab in Fleet.

![Services](./assets/fleet.png "Fleet")

### 2. Add Elastic Agent to Fleet

Note: Don’t confuse Elastic Agent with APM agents–they are different components. In a later step, you’ll instrument your code with APM agents and send the data to an APM Server instance that Elastic Agent spins up.

1. On the Agents tab in Fleet, click Add agent.
2. Under Enroll in Fleet, follow the in-product installation steps.

If installation is successful, you’ll see the agent on the Agents tab in Fleet.

### 3. Add the APM Integration

1. In Kibana, go to Management > Integrations, and search for the Elastic APM integration
2. Click the APM integration to see more details about it, then click Add Elastic APM.
3. On the Add APM integration page, click the down arrow next to Collect application traces. Under Host, define the host and port where APM Server will listen. Inspect or change other settings.
4. Under Apply to agent policy, select the policy created earlier during setup of Fleet.

### 4. Install APM agents for monitoring java aplications deployed over Tomcat

1. Download the agent jar from Maven Central(https://search.maven.org/search?q=a:elastic-apm-agent).
2. Navigate to tomcat directory and add following options to bin/catalina.sh o connect it to AM server

   ```CATALINA_OPTS="$CATALINA_OPTS -javaagent:/path/to/elastic-apm-agent-1.29.0.jar -Delastic.apm.server_url=http://localhost:8200 -Delastic.apm.secret_token=secret -Delastic.apm.environment=production -Delastic.apm.application_packages=org.example"```

### 3. View your data

Back in Kibana, under Observability, select APM. You should see application performance monitoring data flowing into the Elastic Stack!

![Services](./assets/services.png "Services")

Note: If you observe certificate signed by unknown authority in apm-server logs, then follow below steps:

1. View the logs under /opt/Elastic/Agent/data/elastic-agent-da4d9c/logs/default/apm-server-*.ndjson
2. Add below configuration in /opt/Elastic/Agent/data/elastic-agent-da4d9c/install/apm-server-8.1.0-linux-x86_64/apm-server.yml

```
output.elasticsearch:
  hosts: ["localhost:9200"]
  username: "elastic"
  password: "elastic"
  ssl.enabled: true
  ssl.certificate_authorities: ["path/to/ca.crt"]
```
You can get the ca.crt from elasticsearch container path: usr/share/elasticsearch/config/certs
Restart elastic-agent service using

`sudo systemctl restart elastic-agent`

If we need to reinstall elastic-agent 

`sudo /opt/Elastic/Agent/elastic-agent uninstall`